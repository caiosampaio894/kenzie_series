import psycopg2
from dotenv import load_dotenv
import os

load_dotenv()

configs={
    'host': os.environ.get('DB_HOST'),
    'database': os.environ.get('DB_NAME'),
    'user': os.environ.get('DB_USER'),
    'password': os.environ.get('DB_PWD')
}

def create():
    conn = psycopg2.connect(**configs)
    cur = conn.cursor()

    cur.execute("""
        create table if not exists ka_series(
            id BIGSERIAL PRIMARY KEY,
            serie VARCHAR(100) NOT NULL UNIQUE,
            seasons INTEGER NOT NULL,
            released_date DATE NOT NULL,
            genre VARCHAR(50) NOT NULL,
            imdb_rating FLOAT NOT NULL
        );
    """)

    conn.commit()
    cur.close()
    conn.close()

def insert(data):
    conn = psycopg2.connect(**configs)
    cur = conn.cursor()
    create()

    cur.execute("""
        insert into ka_series
            (serie, seasons, released_date, genre, imdb_rating)
        values
            (%(serie)s, %(seasons)s, %(released_date)s, %(genre)s, %(imdb_rating)s)
    """, data, 
    )


    conn.commit()
    cur.close()
    conn.close()




def get_all():
    conn = psycopg2.connect(**configs)
    cur = conn.cursor()
    create()
    cur.execute('select * from ka_series;')

    result = cur.fetchall()
    fieldnames = ["id", "serie", "seasons", "released_date", "genre", "imdb_rating"]
    
    print(result)
    processed_data = [dict(zip(fieldnames, row)) for row in result]

    conn.commit()

    cur.close()
    conn.close()

    return processed_data


def get_by_id(id):
    conn = psycopg2.connect(**configs)
    cur = conn.cursor()
    create()
    cur.execute('select * from ka_series where id=(%s);', (id, ))

    result = [cur.fetchone()]
    print(result)
    fieldnames = ["id", "serie", "seasons", "released_date", "genre", "imdb_rating"]

    processed_data = [dict(zip(fieldnames, row)) for row in result]

    conn.commit()
    cur.close()
    conn.close()
    
    return processed_data



        