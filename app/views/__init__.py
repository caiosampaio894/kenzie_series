
from app.services.services import get_all, get_by_id
from flask import Flask, request
from app.services import  insert

def init_app(app: Flask):

    @app.route('/series', methods=['POST'])
    def create_table():
        data = request.json
        insert(data)

        return data, 201

    @app.route('/series', methods=['GET'])
    def get_data():
        data = get_all()
        
        return {'data': data}, 200

    @app.route('/series/<int:id>', methods=['GET'])
    def get_data_id(id:int):
        data =  get_by_id(id)

        return{'data': data}, 200
    
